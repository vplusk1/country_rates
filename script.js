var api = 'http://www.ringcentral.com/api/index.php';
var loader = document.getElementById('loader');

function showLoader() {
  loader.style.display = 'block';
}

function hideLoader() {
  loader.style.display = 'none';
}

function getCountriesList() {
  var countriesParams = 'cmd=getCountries&typeResponse=json';
  var cbFunc = 'callback=getCountriesListCallback';

  var scriptElement = document.createElement('script');
  scriptElement.src = api + '?' + countriesParams + '&' + cbFunc;
  scriptElement.type = 'text/javascript';
  scriptElement.id = 'countriesRequest';
  document.body.appendChild(scriptElement);
  showLoader();
}

function getCountriesListCallback(jsonResponse) {
  hideLoader();
  if (jsonResponse.status.success !== true) alert ('Something went wrong');
  var countriesList = jsonResponse.result;
  selectEl = document.getElementById('countries');
  baseOption = document.createElement('option');
  baseOption.innerHTML = 'Select country';
  baseOption.value = 'none';
  selectEl.append(baseOption); 
  for (index = 0; index < countriesList.length; index++) {
    opt = document.createElement('option');
    opt.value = countriesList[index].id;
    opt.setAttribute('code', countriesList[index].code);
    opt.setAttribute('country_id', countriesList[index].id)
    opt.setAttribute('shortName', countriesList[index].shortName);
    opt.innerHTML = countriesList[index].name;
    selectEl.appendChild(opt);
  }
}

function chooseCountry(e) {
  e.preventDefault();
  selectEl = document.getElementById('countries');
  var selectedCountryId = selectEl.value;
  if (selectedCountryId == 'none') {
    alert ('Please, choose country from dropdown list');
  } else {
    var countryParams = 'cmd=getInternationalRates&param[internationalRatesRequest][brandId]=1210&param[internationalRatesRequest][countryId]=' +
    selectedCountryId + '&param[internationalRatesRequest][tierId]=3311&typeResponse=json';
    cbFunc = 'callback=chooseCountryCallback';

    var scriptElement = document.createElement('script');
    scriptElement.src = api + '?' + countryParams + '&' + cbFunc;
    scriptElement.type = 'text/javascript';
    scriptElement.id = 'countryRequest';
    document.body.appendChild(scriptElement);
    showLoader();
  } 
  
}

function chooseCountryCallback(jsonResponse) {
  hideLoader();
  if (jsonResponse.status.success !== true) alert ('Something went wrong');
  var rates = jsonResponse.rates[0];
  
  var countryName = rates.key.name;
  var ratesArray = rates.value[0];
  if (typeof ratesArray == undefined) alert ('Selected country has no rates');
  var grouppedRates = [];
  if (ratesArray instanceof Array) {
    for (var rate in ratesArray) {
      var type = ratesArray[rate]['type'];
      var areaCode = ratesArray[rate]['areaCode'];
      var phonePart = ratesArray[rate]['phonePart'];
      var rate = ratesArray[rate]['rate'];
      // protection for cases with empty phonepart
      var code = (phonePart == undefined) ? '' + areaCode : '' + areaCode + phonePart;
   
      // push to final array according to type
      if (!grouppedRates[type]) {
        grouppedRates[type] = [];
      }
      grouppedRates[type]['rate'] = rate;
      grouppedRates[type]['code'] = grouppedRates[type]['code'] ? grouppedRates[type]['code'] : '';
      grouppedRates[type]['code'] += code + ', ';
    }
  } else {
      var type = ratesArray.type;
      var areaCode = ratesArray.areaCode;
      var phonePart = ratesArray.phonePart;
      var rate = ratesArray.rate;
      // preotection for cases with empty phonepart
      var code = (phonePart == undefined) ? '' + areaCode : '' + areaCode + phonePart;
   
      // push to final array according to type 
      if (!grouppedRates[type]) {
        grouppedRates[type] = [];
      }
      grouppedRates[type]['rate'] = rate;
      grouppedRates[type]['code'] = grouppedRates[type]['code'] ? grouppedRates[type]['code'] : '';
      grouppedRates[type]['code'] += code + ', ';
  }
  printTable(countryName, grouppedRates);
}

function printTable(countryName, data) {

  var wrap = document.getElementById('table-wrap');
  wrap.innerHTML = '';
  var table = document.createElement('table');
  var tbody = document.createElement('tbody');
  // console.log('print data', data);
  for (var item in data) {
    // console.log(item);
    tr = document.createElement('tr');
    tr.innerHTML ='<td></td><td>' + item + '</td><td>' +
      data[item].code.slice(0, -2) + '</td><td>$' + data[item].rate + '</td>';
    tbody.appendChild(tr);
  }
  
  thead = generateThead();
  table.appendChild(thead);
  firstRow = tbody.getElementsByTagName('tr')[0];
  firstCell = firstRow.getElementsByTagName('td')[0];
  firstCell.innerHTML = countryName;
  table.appendChild(tbody);
  wrap.appendChild(table);
}

function generateThead() {
  thead = document.createElement('thead');
  theadRow = document.createElement('tr');
  columns = ['Country', 'Type', 'Code', 'Rate'];
  for (i = 0; i < columns.length; i++) {
    th = document.createElement('th');
    th.innerHTML = columns[i];
    th.id = 'col' + columns[i];
    theadRow.appendChild(th);
    thead.appendChild(theadRow);
  }

  return thead;
}

window.onload = function() {
  getCountriesList();
  document.getElementById('searchBtn').addEventListener('click', function (e) {
    chooseCountry(e);
  });
}